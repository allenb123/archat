all: server client

server:
	go build -o archat-server server.go

client:
	go build -o archat client.go
