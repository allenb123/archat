package main

import (
	"bitbucket.org/allenb123/arbit"
	"flag"
	"fmt"
	"github.com/gdamore/tcell"
	"github.com/rivo/tview"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"
)

var adminKey = ""
var users = []string(nil)

var arb *arbit.Client
var (
	app        *tview.Application
	msgView    *tview.TextView
	sendView   *tview.InputField
	headerView *tview.TextView
	nameView   *tview.InputField
	statusView *tview.TextView
	errorView  *tview.TextView
)

func init() {
	flag.StringVar(&adminKey, "key", "", "admin key")
}

// must be called from within main thread
func showError(err string, color string) {
	if color == "" {
		color = "#f44336"
	}
	errorView.SetText(fmt.Sprint(err))
	errorView.SetBackgroundColor(tcell.GetColor(color))
	go func() {
		<-time.After(5 * time.Second)
		app.QueueUpdateDraw(func() {
			errorView.SetText("")
			errorView.SetBackgroundColor(tcell.GetColor("#212121"))
		})
	}()
}

// in ui thread
func handleCommand(text string) {
	fields := strings.Fields(text)
	if len(fields) == 0 {
		return
	}
	switch fields[0] {
	case ":mod":
		if len(fields) == 1 {
			return
		}
		arb.Send("add_mod", fields[1])
		break
	case ":snowflake":
		if len(fields) == 1 {
			return
		}
		arb.Send("add_snowflake", map[string]interface{}{
			"username":  fields[1],
			"snowflake": !(len(fields) >= 3 && fields[2] == "remove"),
		})
		break
	case ":beep":
		arb.Send("beep", nil)
	case ":users":
		showMessage("[#bdbdbd]users: " + strings.Join(users, ", ") + "[-]")
		break
	case ":mute":
		if len(fields) == 1 {
			return
		}
		arb.Send("mute", map[string]interface{}{"username": fields[1], "mute": true})
	case ":unmute":
		if len(fields) == 1 {
			return
		}
		arb.Send("mute", map[string]interface{}{"username": fields[1], "mute": false})
	case ":quit":
		app.Stop()
		break
	case ":delete":
		if len(fields) <= 2 {
			return
		}
		count, err := strconv.Atoi(fields[1])
		if err != nil {
			showError("'"+fields[1]+"' is not a number", "")
			break
		}
		arb.Send("delete", map[string]interface{}{
			"username": fields[2],
			"count":    count,
		})
	case ":help":
		switch statusView.GetText(true) {
		case "admin":
			showError(":quit | :users | :beep | :roomname NAME | :mod USER | :mute USER | :unmute USER | :delete MSGCOUNT USER", "#9c27b0")
		case "mod":
			showError(":quit | :users | :beep | :mute USER | :unmute USER | :delete MSGCOUNT USER", "#9c27b0")
		default:
			showError(":quit | :users | :beep", "#9c27b0")
		}
	case ":roomname":
		if len(fields) == 1 {
			return
		}
		arb.Send("room_name", strings.Join(fields[1:], " "))
	default:
		showError("unknown command. type :help for a list of commands", "")
	}
}

func listen() {
	err := arb.Listen()
	if err != nil {
		app.Stop()
		fmt.Println("\nerror: " + err.Error())
	}
	app.Stop()
	fmt.Println("\nerror: server closed")
}

func showMessage(text string) {
	msgView.SetText(msgView.GetText(false) + text)
}

func statusToFormat(status string) string {
	switch status {
	case "admin":
		return "[#ba68c8]"
	case "mod":
		return "[#ffb74d]"
	case "snowflake":
		return "[#90caf9]"
	default:
		return ""
	}
}

func main() {
	flag.Parse()

	fmt.Print("\x1b]0;Archat\a")

	var err error
	arb, err = arbit.NewClient(flag.Arg(0), http.Header{})
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}

	arb.On("room_users", func(data interface{}) {
		arr, _ := data.([]interface{})
		for _, user := range arr {
			users = append(users, fmt.Sprint(user))
		}
		if len(users) == 1 {
			app.QueueUpdateDraw(func() {
				showMessage("[#00e676]" + users[0] + " is online[-]")
			})
		} else if len(users) > 1 {
			app.QueueUpdateDraw(func() {
				showMessage("[#00e676]" + strings.Join(users, ", ") + " are online[-]")
			})
		}
	})

	arb.On("beep", func(data interface{}) {
		fmt.Print("\a")
	})

	arb.On("message", func(data interface{}) {
		m, ok := data.(map[string]interface{})
		if !ok {
			return
		}

		statusFmt := statusToFormat(fmt.Sprint(m["status"]))
		app.QueueUpdateDraw(func() {
			showMessage(statusFmt + fmt.Sprint(m["username"]) + "[-]: " + fmt.Sprint(m["message"]))
		})
	})

	arb.On("status", func(data interface{}) {
		m, ok := data.(map[string]interface{})
		if !ok {
			return
		}

		statusFmt := statusToFormat(fmt.Sprint(m["status"]))
		app.QueueUpdateDraw(func() {
			showMessage("[#00e676]" + fmt.Sprint(m["username"]) + " is now " + statusFmt + fmt.Sprint(m["status"]) + "[-]")
		})
	})

	arb.On("join", func(data interface{}) {
		users = append(users, fmt.Sprint(data))
		app.QueueUpdateDraw(func() {
			showMessage("[#00e676]" + fmt.Sprint(data) + " joined [-]")
		})
	})

	arb.On("delete", func(data interface{}) {
		m, ok := data.(map[string]interface{})
		if !ok {
			return
		}

		name := fmt.Sprint(m["username"])
		count, ok := m["count"].(float64)
		if !ok || count == 0 {
			return
		}

		app.QueueUpdateDraw(func() {
			deleted := []int(nil)
			text := msgView.GetText(true)
			lines := strings.Split(text, "\n")
			for i := len(lines) - 1; i >= 0; i-- {
				line := lines[i]
				if strings.HasPrefix(line, name+":") {
					deleted = append(deleted, i)
				}
				if len(deleted) >= int(count) {
					break
				}
			}

			text = msgView.GetText(false)
			lines = strings.Split(text, "\n")
			for _, i := range deleted {
				lines[i] = ""
			}
			for i := 0; i < len(lines); i++ {
				if lines[i] == "" {
					lines = append(lines[:i], lines[i+1:]...)
					i--
				}
			}
			msgView.SetText(strings.Join(lines, "\n"))
		})
	})

	arb.On("leave", func(data interface{}) {
		for i, user := range users {
			if user == fmt.Sprint(data) {
				users[i] = ""
			}
		}
		app.QueueUpdateDraw(func() {
			showMessage("[#00e676]" + fmt.Sprint(data) + " left [-]")
		})
	})

	arb.On("error", func(data interface{}) {
		app.QueueUpdateDraw(func() {
			showError(fmt.Sprint(data), "")
		})
	})

	arb.On("user_status", func(data interface{}) {
		app.QueueUpdateDraw(func() {
			if data != nil {
				statusView.SetText(fmt.Sprint(data))
			} else {
				statusView.SetText("")
			}
		})
	})

	arb.On("user_name", func(data interface{}) {
		app.QueueUpdateDraw(func() {
			if data != nil {
				nameView.SetText(fmt.Sprint(data))
			} else {
				nameView.SetText("")
			}
		})
	})

	app = tview.NewApplication()

	msgView = tview.NewTextView().SetDynamicColors(true)
	msgView.SetTextColor(tcell.GetColor("#ffffff")).
		SetBackgroundColor(tcell.GetColor("#212121")).
		SetBorderPadding(1, 1, 3, 3)

	headerView = tview.NewTextView().SetTextAlign(tview.AlignCenter)
	headerView.SetBackgroundColor(tcell.GetColor("#03a9f4")).SetBorderPadding(1, 1, 3, 3)

	arb.On("room_info", func(data interface{}) {
		m, ok := data.(map[string]interface{})
		if !ok {
			return
		}

		colorPrimary := tcell.GetColor(fmt.Sprint(m["theme"]))
		colorDark := tcell.GetColor(fmt.Sprint(m["theme_dark"]))

		app.QueueUpdateDraw(func() {
			headerView.SetText(fmt.Sprint(m["name"]))
			headerView.SetBackgroundColor(colorPrimary)
			nameView.SetFieldBackgroundColor(colorDark).SetBackgroundColor(colorDark)
			statusView.SetBackgroundColor(colorDark)
		})
	})

	subHeaderView := tview.NewFlex().SetDirection(tview.FlexColumn)

	nameView = tview.NewInputField().SetPlaceholder("Enter your username...").
		SetPlaceholderTextColor(tcell.GetColor("#bdbdbd"))
	nameView.SetFieldBackgroundColor(tcell.GetColor("#0288d1")).
		SetBackgroundColor(tcell.GetColor("#0288d1")).SetBorderPadding(0, 0, 3, 3)
	nameView.SetDoneFunc(func(key tcell.Key) {
		if key == tcell.KeyEnter {
			arb.Send("join", map[string]interface{}{
				"username": nameView.GetText(),
				"key":      adminKey,
			})
			app.SetFocus(sendView)
		} else if key == tcell.KeyEscape {
			nameView.SetText("")
		}
	})

	statusView = tview.NewTextView().SetTextAlign(tview.AlignRight)
	statusView.SetBackgroundColor(tcell.GetColor("#0288d1")).SetBorderPadding(0, 0, 3, 3)
	subHeaderView.AddItem(nameView, 0, 1, false).AddItem(statusView, 0, 1, false)

	sendView = tview.NewInputField()
	sendView.SetFieldBackgroundColor(tcell.GetColor("#424242")).
		SetBackgroundColor(tcell.GetColor("#424242")).
		SetBorderPadding(0, 0, 3, 3)
	sendView.SetDoneFunc(func(key tcell.Key) {
		if key == tcell.KeyEnter {
			text := sendView.GetText()
			if len(text) != 0 && text[0] == ':' {
				handleCommand(text)
			} else if text != "" {
				arb.Send("message", map[string]interface{}{"message": text})
			}
			sendView.SetText("")
		} else if key == tcell.KeyEscape {
			sendView.SetText("")
		}
	})

	errorView = tview.NewTextView()
	errorView.SetBackgroundColor(tcell.GetColor("#212121")).SetBorderPadding(0, 0, 3, 3)

	flexView := tview.NewFlex().
		SetDirection(tview.FlexRow)
	flexView.AddItem(headerView, 3, 1, false).
		AddItem(subHeaderView, 1, 1, false).
		AddItem(msgView, 0, 1, false).
		AddItem(errorView, 1, 1, false).
		AddItem(sendView, 1, 1, false)

	go listen()

	if err := app.SetRoot(flexView, true).SetFocus(nameView).Run(); err != nil {
		panic(err)
	}
}
