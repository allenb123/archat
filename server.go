package main

import (
	"bitbucket.org/allenb123/arbit"
	"flag"
	"fmt"
	"net/http"
	"os"
	"strings"
)

type Community struct {
	Admin      *arbit.Client
	Mods       map[*arbit.Client]bool
	Snowflakes map[*arbit.Client]bool
	Muted      map[*arbit.Client]bool
	Names      map[*arbit.Client]string
}

func (comm *Community) Init() {
	comm.Names = make(map[*arbit.Client]string)
	comm.Mods = make(map[*arbit.Client]bool)
	comm.Muted = make(map[*arbit.Client]bool)
	comm.Snowflakes = make(map[*arbit.Client]bool)
}

var roomName string
var roomKey string
var roomTheme string

func init() {
	flag.StringVar(&roomName, "name", "Room", "room name")
	flag.StringVar(&roomKey, "key", "password", "admin key")
	flag.StringVar(&roomTheme, "theme", "#03a9f4 #0288d1", "theme - PRIMARY DARK")
}

func main() {
	flag.Parse()

	comm := Community{}
	comm.Init()

	arb := arbit.NewServer()

	arb.On("join", func(cl *arbit.Client, data interface{}) {
		m, ok := data.(map[string]interface{})
		if !ok {
			return
		}

		theme := strings.Fields(roomTheme)
		cl.Send("room_info", map[string]interface{}{
			"name":       roomName,
			"theme":      theme[0],
			"theme_dark": theme[1],
		})

		users := []string(nil)
		for _, name := range comm.Names {
			users = append(users, name)
		}
		cl.Send("room_users", users)

		name := fmt.Sprint(m["username"])
		if name == "" {
			name = "Anonymous"
		}
		if strings.ContainsAny(name, " :") {
			cl.Send("error", "invalid name")
			return
		}

		comm.Names[cl] = name

		arbit.Broadcast(arb.Clients(), "join", comm.Names[cl])

		if fmt.Sprint(m["key"]) == roomKey && comm.Admin == nil {
			comm.Admin = cl
			cl.Send("user_status", "admin")
			arbit.Broadcast(arb.Clients(), "status", map[string]interface{}{
				"username": name,
				"status":   "admin",
			})
		}
	})

	arb.On("message", func(cl *arbit.Client, data interface{}) {
		if comm.Names[cl] == "" {
			return
		}

		m, ok := data.(map[string]interface{})
		if !ok {
			return
		}

		if comm.Muted[cl] {
			return
		}

		status := ""
		if cl == comm.Admin {
			status = "admin"
		} else if comm.Mods[cl] {
			status = "mod"
		} else if comm.Snowflakes[cl] {
			status = "snowflake"
		}

		str := fmt.Sprint(m["message"])

		arbit.Broadcast(arb.Clients(), "message", map[string]interface{}{
			"message":  str,
			"username": comm.Names[cl],
			"status":   status,
		})
	})

	arb.On("beep", func(cl *arbit.Client, data interface{}) {
		if !comm.Muted[cl] {
			arbit.Broadcast(arb.Clients(), "beep", nil)
		}
	})

	arb.OnClose(func(cl *arbit.Client, code int) {
		if comm.Names[cl] == "" {
			return
		}

		arbit.Broadcast(arb.Clients(), "leave", comm.Names[cl])

		if comm.Admin == cl {
			comm.Admin = nil
		}
		delete(comm.Mods, cl)
		delete(comm.Snowflakes, cl)
		delete(comm.Names, cl)
	})

	arb.On("add_mod", func(cl *arbit.Client, data interface{}) {
		if comm.Admin == cl {
			found := false
			targetName := fmt.Sprint(data)
			for otherCl, name := range comm.Names {
				if targetName == name && cl != otherCl && !comm.Mods[otherCl] {
					comm.Mods[otherCl] = true
					delete(comm.Snowflakes, otherCl)
					otherCl.Send("user_status", "mod")
					found = true
				}
			}
			if found {
				arbit.Broadcast(arb.Clients(), "status", map[string]interface{}{
					"username": targetName,
					"status":   "mod",
				})
			} else {
				cl.Send("error", targetName+" either has a role or does not exist")
			}
		} else {
			cl.Send("error", "admin required to assign mod")
		}
	})

	arb.On("add_snowflake", func(cl *arbit.Client, data interface{}) {
		if comm.Admin != cl {
			cl.Send("error", "admin required to assign snowflake")
			return
		}

		m, ok := data.(map[string]interface{})
		if !ok {
			return
		}

		found := false
		targetName := fmt.Sprint(m["username"])
		isSnowflake := m["snowflake"] == true
		for otherCl, name := range comm.Names {
			if targetName == name && cl != otherCl && !comm.Mods[otherCl] {
				comm.Snowflakes[otherCl] = isSnowflake
				if isSnowflake {
					otherCl.Send("user_status", "snowflake")
				}
				found = true
			}
		}
		if !found {
			cl.Send("error", targetName+" either has another role or does not exist")
			return
		}

		if isSnowflake {
			arbit.Broadcast(arb.Clients(), "status", map[string]interface{}{
				"username": targetName,
				"status":   "snowflake",
			})
		}
	})

	arb.On("room_name", func(cl *arbit.Client, data interface{}) {
		if comm.Admin != cl {
			cl.Send("error", "admin required to set room name")
			return
		}

		roomName = fmt.Sprint(data)

		theme := strings.Fields(roomTheme)
		arbit.Broadcast(arb.Clients(), "room_info", map[string]interface{}{
			"name":       roomName,
			"theme":      theme[0],
			"theme_dark": theme[1],
		})
	})

	arb.On("mute", func(cl *arbit.Client, data interface{}) {
		if comm.Admin != cl && !comm.Mods[cl] {
			cl.Send("error", "admin or mod required to mute")
			return
		}

		m, ok := data.(map[string]interface{})
		if !ok {
			return
		}

		targetName := fmt.Sprint(m["username"])
		mute := m["mute"] == true
		found := false
		for othercl, name := range comm.Names {
			if name == targetName {
				comm.Muted[othercl] = mute
				found = true
			}
		}
		if !found {
			cl.Send("error", targetName+" does not exist")
		}
	})

	arb.On("delete", func(cl *arbit.Client, data interface{}) {
		if comm.Admin == cl || comm.Mods[cl] {
			m, ok := data.(map[string]interface{})
			if !ok {
				return
			}

			name := fmt.Sprint(m["username"])
			count, ok := m["count"].(float64)
			if !ok {
				return
			}

			arbit.Broadcast(arb.Clients(), "delete", map[string]interface{}{
				"username": name,
				"count":    count,
			})
		} else {
			cl.Send("error", "admin or mod required to delete messages")
		}
	})

	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}

	fmt.Println("ws://localhost:" + port + "/ws")
	http.Handle("/ws", arb)
	http.ListenAndServe(":"+port, nil)
}
